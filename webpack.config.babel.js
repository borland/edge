import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import fs from 'fs';
import glob from 'glob';
import path from 'path';
import pkg from './package.json';

const esBabelLoaderOptions = require('./babel.config');

const babelAlias = {
  ...esBabelLoaderOptions.plugins
    .find(plugin => Array.isArray(plugin) && plugin[0] === 'module-resolver')[1].alias
};

Object.keys(babelAlias).forEach(alias => {
  babelAlias[alias] = `${babelAlias[alias].replace(/\.\/src\/modules/, path.resolve('dist/es'))}/`;
});

const copyFolder = (source, target, exclusions) => {
  !fs.existsSync(target) && fs.mkdirSync(target);
  fs.readdirSync(source).forEach(item => {
    const sourcePath = path.join(source, item);
    if (!exclusions.find(re => re.test(sourcePath))) {
      const targetPath = path.join(target, item);
      if (fs.lstatSync(sourcePath).isDirectory()) {
        copyFolder(sourcePath, targetPath, exclusions);
      } else {
        const resolvedFile = Object.keys(babelAlias).reduce(
          (file, alias) => file.replace(RegExp(`import (.+) from '${alias}(.+)'`, 'g'), (match, p1, p2) => {
            const relativePath =
              Array(targetPath.replace(babelAlias[alias], '').split('/').length - 1).fill('..').join('/');
            return `import ${p1} from '${relativePath}${p2}'`;
          }),
          fs.readFileSync(sourcePath, 'utf-8')
        );
        fs.writeFileSync(targetPath, resolvedFile, 'utf-8');
      }
    }
  });
};

class PostBuildPlugin {
  apply = compiler => {
    compiler.hooks.afterEmit.tap('PostBuildPlugin', () => {
      const kb = fileSize => `${(fileSize / 1024).toFixed(1)} KB`;
      let readme = fs.readFileSync('README.md', 'utf-8');
      let mainPackageSize = 0;
      let otherPackageSize = 0;
      fs.readdirSync('dist').forEach(file => {
        if (/\.js$/.test(file)) {
          const fileSize = fs.statSync(`dist/${file}`).size;
          if (file === 'gnar-edge.js') { mainPackageSize += fileSize; } else { otherPackageSize += fileSize; }
          readme = readme.replace(RegExp(`{{ ${file.replace(/\.js$/, '')} package size }}`, 'g'), kb(fileSize));
        }
      });
      readme = readme.replace('{{ package size comparison }}', `${kb(mainPackageSize)} vs. ${kb(otherPackageSize)}`);
      fs.writeFileSync('dist/README.md', readme);
      const excludes = [ /\/__tests__$/, /\/\$mocks$/, /\/gnar-edge.js$/ ];
      copyFolder(path.resolve('src/modules'), path.resolve('dist/es'), excludes);
    });
  }
}

export default (env = {}) => {
  const isProd = env.production;

  const distPkg = [
    'name',
    'version',
    'author',
    'license',
    'homepage',
    'repository',
    'description',
    'keywords',
    'main'
  ].reduce((memo, key) => {
    memo[key] = pkg[key];
    return memo;
  }, {});

  fs.writeFileSync('./dist/package.json', `${JSON.stringify(distPkg, null, 2)}\n`);

  const externals = [
    '@material-ui/core/colors/amber',
    '@material-ui/core/colors/green',
    '@material-ui/core/IconButton',
    '@material-ui/core/Snackbar',
    '@material-ui/core/SnackbarContent',
    '@material-ui/core/styles',
    '@material-ui/icons/CheckCircle',
    '@material-ui/icons/Close',
    '@material-ui/icons/Error',
    '@material-ui/icons/Info',
    '@material-ui/icons/Warning',
    'animate.css/source/zooming_exits/zoomOutUp.css',
    'classnames',
    'immutable',
    'prop-types',
    'react',
    'react-hot-loader',
    'react-redux',
    'redux',
    'redux-actions',
    'redux-saga/effects'
  ].reduce((memo, dep) => {
    memo[dep] = `umd ${dep}`;
    return memo;
  }, {});

  return {
    devtool: !isProd && 'inline-source-map',
    entry: glob.sync('./src/packages/*.js').reduce((memo, fileName) => {
      const packageName = fileName.match(/\/([\w-]+)\.js$/)[1];
      memo[packageName] = fs.statSync(fileName).size
        ? fileName
        : fileName.replace('packages', 'modules').replace('.js', '');
      return memo;
    }, {}),
    externals,
    mode: isProd ? 'production' : 'development',
    module: {
      rules: [ {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      } ]
    },
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          sourceMap: !isProd
        })
      ]
    },
    output: {
      path: path.resolve('dist'),
      library: 'gnar-edge',
      libraryTarget: 'umd',
      globalObject: "typeof self === 'undefined' ? this : self"
    },
    plugins: [ new PostBuildPlugin() ],
    resolve: {
      extensions: [ '.js', '.jsx' ]
    }
  };
};
