import base64 from './base64';
import getJwt from './getJwt';
import isLoggedIn from './isLoggedIn';
import jwtDecode from './jwtDecode';
import {
  ADD_NOTIFICATION,
  DISMISS_NOTIFICATION,
  dismissNotification,
  notificationActions,
  notifications as _notifications,
  Notifications,
  notifyError,
  notifyInfo,
  notifySuccess,
  notifyWarning
} from './notifications';

import { gnarActions, gnarReducers } from './redux';

export { base64 };
export drain from './drain';
export handleChange from './handleChange';

export const jwt = {
  base64,
  jwtDecode,
  getJwt,
  isLoggedIn
};

export const notifications = {
  ADD_NOTIFICATION,
  DISMISS_NOTIFICATION,
  dismissNotification,
  notificationActions,
  notifications: _notifications,
  Notifications,
  notifyError,
  notifyInfo,
  notifySuccess,
  notifyWarning
};

export const redux = { gnarActions, gnarReducers };
