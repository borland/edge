import { createAction } from 'redux-actions';

const isObject = obj => obj && typeof obj === 'object' && obj.constructor === Object;

const simpleAction = (actionName, keys) =>
  createAction(actionName, (...params) => keys.reduce((memo, key, idx) => {
    memo[key] = params[idx];
    return memo;
  }, {}));

const parseActions = obj => Object.keys(obj).reduce((memo, key) => {
  const val = obj[key];
  const valType = typeof val;
  if (/^([A-Z\d]+_)*[A-Z\d]+$/.test(key) && val && ([ 'function', 'string' ].includes(valType) || Array.isArray(val))) {
    const actionName = key.toLowerCase().replace(/_[a-z]/g, m => m[1].toUpperCase());
    if (valType === 'function') {
      memo[actionName] = createAction(key, val);
    } else {
      const valArray = valType === 'string' ? [ val ] : val;
      memo[actionName] = simpleAction(key, valArray);
    }
  } else {
    memo[key] = isObject(val) ? parseActions(val) : val;
  }
  return memo;
}, {});

export default parseActions;
