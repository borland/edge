import jwtDecode from './jwtDecode';

export default keyName => {
  try {
    return jwtDecode(localStorage.getItem(keyName || 'jwt'));
  } catch (e) {
    return null;
  }
};
