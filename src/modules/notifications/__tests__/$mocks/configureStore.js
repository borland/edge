import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { notifications } from 'notifications';

const rootReducer = combineReducers({
  notifications
});

function* sagas() {}  // eslint-disable-line no-empty-function

export default () => {
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(rootReducer, compose(applyMiddleware(sagaMiddleware)));

  sagaMiddleware.run(sagas);

  return store;
};
