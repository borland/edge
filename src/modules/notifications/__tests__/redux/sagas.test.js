import drain from 'modules/drain';

import {
  ADD_NOTIFICATION,
  DISMISS_NOTIFICATION,
  dismissNotification,
  notifyError,
  notifyInfo,
  notifySuccess,
  notifyWarning
} from 'notifications';
import { notificationMessages } from 'notifications-mocks/constants';

const mockPut = jest.fn();

jest.mock('redux-saga/effects', () => ({
  call: (fn, ...args) => fn(...args),
  async put(...args) { return mockPut(...args); }
}));

describe('notifications util', () => {
  it('dispatches the correct action with notifyError', drain(function* () {
    const message = notificationMessages.error;
    yield notifyError(message);
    expect(mockPut).toHaveBeenCalledWith({ type: ADD_NOTIFICATION, payload: { message, variant: 'error' } });
  }));

  it('dispatches the correct action with notifyInfo', drain(function* () {
    const message = notificationMessages.info;
    yield notifyInfo(message);
    expect(mockPut).toHaveBeenCalledWith({ type: ADD_NOTIFICATION, payload: { message, variant: 'info' } });
  }));

  it('dispatches the correct action with notifySuccess', drain(function* () {
    const message = notificationMessages.success;
    yield notifySuccess(message);
    expect(mockPut).toHaveBeenCalledWith({ type: ADD_NOTIFICATION, payload: { message, variant: 'success' } });
  }));

  it('dispatches the correct action with notifyWarning', drain(function* () {
    const message = notificationMessages.warning;
    yield notifyWarning(message);
    expect(mockPut).toHaveBeenCalledWith({ type: ADD_NOTIFICATION, payload: { message, variant: 'warning' } });
  }));

  it('dispatches the correct action with notifySuccess and a set of options', drain(function* () {
    const message = notificationMessages.success;
    const opts = { autoDismissMillis: 1, key: 'to the kingdom', onDismiss: () => {} };
    yield notifySuccess(message, opts);
    expect(mockPut).toHaveBeenCalledWith(
      { type: ADD_NOTIFICATION, payload: { message, ...opts, variant: 'success' } }
    );
  }));

  it('dismisses the notification with dismissNotification', drain(function* () {
    const key = 'testKey';
    const message = notificationMessages.success;
    yield notifySuccess(message, { key });
    expect(mockPut).toHaveBeenCalledWith({ type: ADD_NOTIFICATION, payload: { key, message, variant: 'success' } });
    yield dismissNotification(key);
    expect(mockPut).toHaveBeenCalledWith({ type: DISMISS_NOTIFICATION, payload: { key } });
  }));
});
