import { createAction } from 'redux-actions';

import { ADD_NOTIFICATION, DISMISS_NOTIFICATION } from './reducers';

export default {
  dismissNotification: createAction(DISMISS_NOTIFICATION, key => ({ key })),
  notifyError: createAction(ADD_NOTIFICATION, (message, opts = {}) => ({ message, ...opts, variant: 'error' })),
  notifyInfo: createAction(ADD_NOTIFICATION, (message, opts = {}) => ({ message, ...opts, variant: 'info' })),
  notifySuccess: createAction(ADD_NOTIFICATION, (message, opts = {}) => ({ message, ...opts, variant: 'success' })),
  notifyWarning: createAction(ADD_NOTIFICATION, (message, opts = {}) => ({ message, ...opts, variant: 'warning' }))
};
