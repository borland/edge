import { put } from 'redux-saga/effects';

import { ADD_NOTIFICATION, DISMISS_NOTIFICATION } from './reducers';

function* addNotification(message, opts, variant) {
  yield put({ type: ADD_NOTIFICATION, payload: { message, ...opts, variant } });
}

export function* notifyError(message, opts = {}) {
  yield addNotification(message, opts, 'error');
}

export function* notifyInfo(message, opts = {}) {
  yield addNotification(message, opts, 'info');
}

export function* notifySuccess(message, opts = {}) {
  yield addNotification(message, opts, 'success');
}

export function* notifyWarning(message, opts = {}) {
  yield addNotification(message, opts, 'warning');
}

export function* dismissNotification(key) {
  yield put({ type: DISMISS_NOTIFICATION, payload: { key } });
}
