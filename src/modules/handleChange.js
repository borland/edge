export default function (name, cb, options = {}) {
  return e => {
    const { beforeSetState } = options;
    beforeSetState && beforeSetState();
    const value = Object.prototype.hasOwnProperty.call(e, 'target')
      ? Object.prototype.hasOwnProperty.call(e.target, 'checked')
        ? e.target.checked
        : e.target.value
      : e;
    this.setState({ [name]: value }, ...cb ? [ cb ] : []);
  };
}
