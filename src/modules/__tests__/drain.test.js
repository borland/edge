import drain from 'modules/drain';

const now = new Date();

describe('drain', () => {
  describe('invokes properly', () => {
    it('requires a generator function', () => {
      expect(() => drain('bad type')).toThrowError('Drain expects a generator function - received "bad type"');
    });

    it('passes the rest of the arguments', () => {
      const args = {
        arr: [ 42 ],
        fun: () => {},
        num: 42,
        obj: { value: 42 },
        str: 'forty-teo'
      };
      drain(
        function* (arr, fun, num, obj, str) {
          yield () => {
            expect(arr).toBe(args.arr);
            expect(fun).toBe(args.fun);
            expect(num).toBe(args.num);
            expect(obj).toBe(args.obj);
            expect(str).toBe(args.str);
          };
        },
        ...Object.values(args)
      );
    });
  });

  describe('handles simple values properly', () => {
    it('handles numbers', drain(function* () {
      const res = yield 1;
      expect(res).toEqual(1);
    }));

    it('handles strings', drain(function* () {
      const res = yield 'Dexter';
      expect(res).toEqual('Dexter');
    }));

    it('handles booleans', drain(function* () {
      const res = yield true;
      expect(res).toEqual(true);
    }));

    it('handles dates', drain(function* () {
      const res = yield now;
      expect(res).toEqual(now);
    }));

    it('handles null', drain(function* () {
      const res = yield null;
      expect(res).toEqual(null);
    }));

    it('handles undefined', drain(function* () {
      const res = yield undefined;
      expect(res).toEqual(undefined);
    }));

    it('handles NaN', drain(function* () {
      const res = yield NaN;
      expect(res).toEqual(NaN);
    }));

    it('handles symbols', drain(function* () {
      const res = yield Symbol.Dexter;
      expect(res).toEqual(Symbol.Dexter);
    }));
  });

  describe('handles arrays properly', () => {
    it('handles an empty array', drain(function* () {
      const res = yield [];
      yield () => {
        expect(res).toEqual([]);
      };
    }));

    it('handles an array of promises', drain(function* () {
      const res = yield [ Promise.resolve(1), Promise.resolve(2), Promise.resolve(3) ];
      yield () => {
        expect(res).toEqual([ 1, 2, 3 ]);
      };
    }));

    it('handles an array of generators', drain(function* () {
      const res = yield [ function* () { yield 0; return 1; } ];
      yield () => {
        expect(res).toEqual([ 1 ]);
      };
    }));

    it('handles an array of slow generators', drain(function* () {
      function* work() {
        yield setTimeout(() => {}, 50);
        return 'yay';
      }
      const res = yield [ work, work, work ];
      yield () => {
        expect(res).toEqual([ 'yay', 'yay', 'yay' ]);
      };
    }));
  });

  describe('handles objects properly', () => {
    it('handles an empty object', drain(function* () {
      const res = yield {};
      expect(res).toEqual({});
    }));

    it('handles an object of promises', drain(function* () {
      const res = yield {
        a: Promise.resolve(1),
        b: Promise.resolve(2),
        c: Promise.resolve(3)
      };
      expect(res).toEqual({ a: 1, b: 2, c: 3 });
    }));

    it('handles an object of varied types', drain(function* () {
      class Pet {
        constructor(name) {
          this.name = name;
        }
      }
      const testObj = {
        name: { first: 'Dexter' },
        age: 7,
        address: 'Redwood City, California',
        dexter: new Pet('Dexter'),
        now,
        falsey: false,
        nully: null,
        undefiney: undefined
      };
      const res = yield testObj;
      expect(res).toEqual(testObj);
    }));

    it('preserves object key order', drain(function* () {
      const timedThunk = time => () => setTimeout(() => {}, time);
      const before = {
        sun: timedThunk(30),
        rain: timedThunk(20),
        moon: timedThunk(10)
      };
      const after = yield before;
      expect(Object.keys(before)).toEqual(Object.keys(after));
    }));
  });

  describe('handles recursion properly', () => {
    it('aggregates an array of promises', drain(function* () {
      const res = yield [ Promise.resolve(1), [ Promise.resolve(2), Promise.resolve(3) ] ];
      expect(res).toEqual([ 1, [ 2, 3 ] ]);
    }));

    it('aggregates arrays of arrays', drain(function* () {
      const res = yield {
        a: Promise.resolve(1),
        b: {
          c: Promise.resolve(2),
          d: Promise.resolve(3)
        }
      };
      expect(res).toEqual({ a: 1, b: { c: 2, d: 3 } });
    }));
  });

  describe('handles promises properly', () => {
    const getPromise = (val, err) =>
      new Promise((resolve, reject) => {
        if (err) { reject(err); } else { resolve(val); }
      });

    it('handles a simple promies', drain(function* () {
      const res = yield getPromise(1);
      expect(res).toEqual(1);
    }));

    it('handles a series of promises', drain(function* () {
      const a = yield getPromise(1);
      const b = yield getPromise(2);
      const c = yield getPromise(3);
      expect([ a, b, c ]).toEqual([ 1, 2, 3 ]);
    }));

    it('handles exceptions', drain(function* () {
      const error = new Error('boom');
      try {
        yield getPromise(1, error);
      } catch (e) {
        expect(e).toEqual(error);
      }
      const res = yield getPromise(1);
      expect(res).toEqual(1);
    }));
  });

  describe('handles generators properly', () => {
    function* generatorFunction(x) { return yield x; }

    it('handles a simple generator', drain(function* () {
      const res = yield generatorFunction(10);
      expect(res).toEqual(10);
    }));

    it('handles nested generators', drain(function* () {
      const res = yield generatorFunction(generatorFunction(10));
      expect(res).toEqual(10);
    }));
  });

  describe('handles thunks properly', () => {
    const getThunk = (val, err, error) =>
      () => {
        if (error) { throw error; }
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            if (err) { reject(err); } else { resolve(val); }
          }, 10);
        });
      };

    it('handles a simple thunk', drain(function* () {
      const res = yield getThunk(1);
      expect(res).toEqual(1);
    }));

    it('handles a series of thunks', drain(function* () {
      const a = yield getThunk(1);
      const b = yield getThunk(2);
      const c = yield getThunk(3);
      expect([ a, b, c ]).toEqual([ 1, 2, 3 ]);
    }));

    it('handles exceptions', drain(function* () {
      const error = new Error('boom');
      try {
        yield getThunk(1, error);
      } catch (e) {
        expect(e).toEqual(error);
      }
      try {
        yield getThunk(1, null, error);
      } catch (e) {
        expect(e).toEqual(error);
      }
      const res = yield getThunk(1);
      expect(res).toEqual(1);
    }));

    it('handles thunks in nested drains', drain(function* () {
      const hits = [];

      return yield drain(function* () {
        const a1 = yield getThunk(1);
        const b1 = yield getThunk(2);
        const c1 = yield getThunk(3);
        hits.push('one');

        expect([ a1, b1, c1 ]).toEqual([ 1, 2, 3 ]);

        yield drain(function* () {
          hits.push('two');
          const a2 = yield getThunk(1);
          const b2 = yield getThunk(2);
          const c2 = yield getThunk(3);

          expect([ a2, b2, c2 ]).toEqual([ 1, 2, 3 ]);

          yield drain(function* () {
            hits.push('three');
            const a3 = yield getThunk(1);
            const b3 = yield getThunk(2);
            const c3 = yield getThunk(3);

            expect([ a3, b3, c3 ]).toEqual([ 1, 2, 3 ]);
          });
        });

        yield drain(function* () {
          hits.push('four');
          const a4 = yield getThunk(1);
          const b4 = yield getThunk(2);
          const c4 = yield getThunk(3);

          expect([ a4, b4, c4 ]).toEqual([ 1, 2, 3 ]);
        });

        expect(hits).toEqual([ 'one', 'two', 'three', 'four' ]);
      });
    }));

    it("passes yielded thunk values into drain's promise chain", () => {
      drain(function* () {
        return yield [
          yield getThunk(1),
          yield getThunk(2),
          yield getThunk(3),
          ...yield [
            yield getThunk(4),
            yield getThunk(5),
            yield getThunk(6)
          ]
        ];
      }).then(data => {
        expect(data).toEqual([ 1, 2, 3, 4, 5, 6 ]);
      });
    });

    it('handles sequential thunk exceptions', drain(function* () {
      const errors = [];
      try {
        yield getThunk(1, new Error('foo'));
      } catch (err) {
        errors.push(err.message);
      }

      try {
        yield getThunk(1, new Error('bar'));
      } catch (err) {
        errors.push(err.message);
      }

      expect(errors).toEqual([ 'foo', 'bar' ]);
    }));

    it('handles sequential thunk rejections', drain(function* () {
      const errors = [];
      try {
        yield getThunk(1, null, new Error('foo'));
      } catch (err) {
        errors.push(err.message);
      }

      try {
        yield getThunk(1, null, new Error('bar'));
      } catch (err) {
        errors.push(err.message);
      }

      expect(errors).toEqual([ 'foo', 'bar' ]);
    }));

    it("passes immediate think errors into the drain's promise chain", () => {
      const exception = new Error('fail');
      drain(function* () {
        yield getThunk(1);
        yield getThunk(2, null, exception);
        yield getThunk(3);
      }).catch(err => {
        expect(err).toBe(exception);
      });
    });

    it("passes future think errors into the drain's promise chain", () => {
      const exception = new Error('fail');
      drain(function* () {
        yield getThunk(1);
        yield getThunk(2, exception);
        yield getThunk(3);
      }).catch(err => {
        expect(err).toBe(exception);
      });
    });
  });

  describe('handles async functions properly', () => {
    const getPromise = (x, err) =>
      new Promise((resolve, reject) => setTimeout(() => { if (err) { reject(err); } else { resolve(2 * x); } }, 10));

    async function testAsync(x, err) {
      return getPromise(x, err);
    }

    async function testAsyncAwait(x, err) {
      try {
        return await getPromise(x, err);
      } catch (e) {
        return e;
      }
    }

    it('handles a simple async function', drain(function* () {
      const res = yield testAsync(10);
      expect(res).toBe(20);
    }));

    it('handles a simple async function rejection', drain(function* () {
      try {
        yield testAsync(10, 'failed');
      } catch (e) {
        expect(e).toBe('failed');
      }
    }));

    it('handles a simple async await function', drain(function* () {
      const res = yield testAsyncAwait(10);
      expect(res).toBe(20);
    }));

    it('handles a simple async await function rejection', drain(function* () {
      const res = yield testAsyncAwait(10, 'failed');
      expect(res).toBe('failed');
    }));
  });

  describe('handles a mix of yielded types properly', () => {
    drain(function* () {
      let result = 1;
      result *= yield 2;
      const array = yield [ 3 ];
      result *= array[0];
      const object = yield { x: 4 };
      result *= object.x;
      result *= yield new Promise(resolve => { setTimeout(() => { resolve(5); }, 10); });
      result *= yield () => 6;
      result *= yield () => new Promise(resolve => { setTimeout(() => { resolve(7); }, 10); });
      const mixedArray = yield [
        8,
        new Promise(resolve => { setTimeout(() => { resolve(9); }, 10); }),
        () => new Promise(resolve => { setTimeout(() => { resolve(10); }, 10); })
      ];
      mixedArray.forEach(x => { result *= x; });
      const mixedObject = yield {
        a: 11,
        b: new Promise(resolve => { setTimeout(() => { resolve(12); }, 10); }),
        c: () => new Promise(resolve => { setTimeout(() => { resolve(13); }, 10); })
      };
      Object.values(mixedObject).forEach(x => { result *= x; });
      function* generatorFunction1() {
        return yield 14;
      }
      result *= yield generatorFunction1;
      function* generatorFunction2(x) {
        return yield x;
      }
      const generator = generatorFunction2(15);
      result *= yield generator;
      result *= yield async () => {
        try {
          return await new Promise(resolve => { setTimeout(() => { resolve(16); }, 10); });
        } catch (e) {
          throw e;
        }
      };
      return result;
    }).then(result => { expect(result).toBe(20922789888000); /* 16! */ });
  });

  describe('handles chaining properly', () => {
    it('handles simeple data', () => {
      drain(function* () {
        return yield 1;
      }).then(data => {
        expect(data).toEqual(1);
      });
    });

    it('handles promises', () => {
      drain(function* () {
        return yield Promise.resolve(1);
      }).then(data => {
        expect(data).toEqual(1);
      });
    });

    it('handles exceptions', () => {
      const exception = new Error('boom');
      drain(function* () {
        yield 1;
        throw exception;
      }).then(() => {
        throw new Error('nope');
      }).catch(error => {
        expect(error).toEqual(exception);
      });
    });

    it('handles rejected Promises', () => {
      const exception = Error('oops');
      drain(function* () {
        return yield Promise.reject(exception);
      }).catch(error => {
        expect(error).toEqual(exception);
      });
    });

    it('handles rejected Promises', () => {
      const exception = Error('oops');
      drain(function* () {
        return yield Promise.reject(exception);
      }).catch(error => {
        expect(error).toEqual(exception);
      });
    });

    it('handles Finaly blocks in Promises', () => {
      const fn = jest.fn();
      drain(function* () {
        return yield 'hi mom!';
      })
        .finally(fn)
        .then(() => { expect(fn).toHaveBeenCalled(); });
    });
  });

  describe('handles exceptions properly', () => {
    it('catches errors', () => {
      drain(function* () {
        yield function* () {
          yield 0;
          throw new Error('boom');
        };
      }).catch(err => {
        expect(err).toEqual(new Error('boom'));
      });
    });

    it('handles exceptions in the catch block', () => {
      drain(function* () {
        try {
          yield 0;
          throw new Error('first');
        } catch (e) {
          throw new Error('second');
        }
      }).catch(err => {
        expect(err).toEqual(new Error('second'));
      });
    });

    it('handles nested exceptions in the catch block', () => {
      drain(function* () {
        try {
          yield 0;
          throw new Error('first');
        } catch (e1) {
          try {
            throw new Error('second');
          } catch (e2) {
            throw new Error('third');
          }
        }
      }).catch(err => {
        expect(err).toEqual(new Error('third'));
      });
    });

    it('handles exceptions in the catch block', () => {
      drain(function* () {
        try {
          yield 0;
          throw new Error('first');
        } catch (e) {
          throw new Error('second');
        }
      }).catch(err => {
        expect(err).toEqual(new Error('second'));
      });
    });

    it('allows handled exceptions', drain(function* () {
      yield function* () {
        try {
          yield null;
          throw new Error('lol');
        } catch (err) {
          expect(err).toEqual(new Error('lol'));
        }
      };
    }));

    it("passes exceptions into drain's promise chain", () => {
      drain(function* () {
        yield null;
        throw new Error('fail');
      }).catch(error => {
        expect(error).toEqual(Error('fail'));
      });
    });
  });
});
