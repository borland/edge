import { LocalStorageMock } from 'mocks';
import { expiredJwt, jwt } from 'mocks/constants';
import isLoggedIn from 'modules/isLoggedIn';

describe('isLoggedIn', () => {
  it('returns true when a jwt exists in localStorage (default keyName) and has not expired', () => {
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', jwt);
    expect(isLoggedIn()).toBe(true);
    localStorageMock.deactivate();
  });

  it('returns true when a jwt exists in localStorage (custom keyName) and has not expired', () => {
    const keyName = 'the wizard of oz';
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem(keyName, jwt);
    expect(isLoggedIn(keyName)).toBe(true);
    localStorageMock.deactivate();
  });

  it('returns false when a jwt exists in localStorage and has expired', () => {
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', expiredJwt);
    expect(isLoggedIn()).toBe(false);
    localStorageMock.deactivate();
  });

  it("returns false when a jwt doen't exist in localStorage", () => {
    const localStorageMock = new LocalStorageMock();
    expect(isLoggedIn()).toBe(false);
    localStorageMock.deactivate();
  });
});
